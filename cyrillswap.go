// echo 'Amor Fati' | go run cyrillswap.go

package main

import (
	"bufio"
	"bytes"
	"fmt"
	"os"
)

func main() {
	userString := bufio.NewReader(os.Stdin)
	line, err := userString.ReadString('\n')

	if err != nil {
		panic(err)
	}

	fmt.Println(CyrillicSwap(line))
}

func CyrillicSwap(latinString string) string {
	swapMap := map[rune]rune{
		'a': 'а', // \u0430
		'c': 'с', // \u0441
		'e': 'е', // \u0435
		'o': 'о', // \u043e
		'p': 'р', // \u0440
		'x': 'х', // \u0445
		'y': 'у', // \u0443
		'A': 'А', // \u0410
		'B': 'В', // \u0412
		'C': 'С', // \u0421
		'H': 'Н', // \u041d
		'E': 'Е', // \u0415
		'K': 'К', // \u041a
		'M': 'М', // \u041c
		'O': 'О', // \u041e
		'P': 'Р'} // \u0420

	var swapBuffer bytes.Buffer
	for _, c := range latinString {
		// check if latin rune has counterpart in map
		if val, ok := swapMap[c]; ok {
			// swap latin rune for its cyrillic counterpart
			swapBuffer.WriteRune(val)
		} else {
			swapBuffer.WriteRune(c)
		}
	}

	return swapBuffer.String()
}
